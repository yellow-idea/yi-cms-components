# yi-cms-components

# 🔧 🔧 Update: 16/03/2021 ❗️❗️

## Nav Bar

- สามารถใส่ :showCRMLogo="true" เพิ่มแสดงรูปภาพ Logo My CRM. ใน Nav Bar


# 🔧 🔧 Update: 09/03/2021 ❗️❗️

## Data table

- สามารถใส่ :loading="true/false" ให้กับ <data-table :loading="true/false" ></data-table> ได้

## Add Message, Rich Menu, Upload image preview, Upload video preview
- ต้องใส่!! :uploadUrl="api Upload image url" เพื่อให้สามารถ upload รูปได้






## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


