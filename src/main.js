import Vue from 'vue'
import { Plugin } from 'vue-fragment'
import Toast from 'vue-toastification'
import VueClipboard from 'vue-clipboard2'
import CKEditor from '@ckeditor/ckeditor5-vue2'
import 'vue-toastification/dist/index.css'

import App from './App.vue'
import store from './store'
import router from './router'

import vuetify from '@/plugins/vuetify'
import 'vuetify/dist/vuetify.min.css'

import "@/styles/index.scss"

const optionsToast = {
  timeout: 3000,
  closeOnClick: true,
  pauseOnHover: true,
}

Vue.config.productionTip = false
Vue.use(Plugin)
Vue.use(VueClipboard)
Vue.use(Toast, optionsToast)
Vue.use(CKEditor)

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
