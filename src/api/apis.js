import { service } from "./axios"

const request = async(instance , { cbSuccess , cbError }) => {
    try {
        const res = await instance
        if(res && res.status === 200 ) {
            cbSuccess(res)
        } else {
            cbError(res , "Error !")
        }
    } catch( e ) {
        console.log(e)
        cbError(null , e)
    }
}

export const listing = ({ payload, cbSuccess, cbError }) => request(
    service.post(`https://api-yellow-cms-v4-fvo3wkanea-as.a.run.app/api/cms/line-greeting/list`, payload),
    { cbSuccess, cbError }
)