import axios from "axios"

export const service = axios.create({
    baseURL : process.env.VUE_APP_API_END_POINT,
    // withCredentials: true,
    // timeout : 5000
})

service.interceptors.request.use(
    config => {
        return config
    } ,
    error => {
        console.log(error)
        return Promise.reject(error)
    }
)


